package trial.toptal.todo.network;

import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

/*
	This class defines API related constants and exposes basic network
	functionalities
*/
public class TodoAPI
{
	public static String APP_ID = "rH7CNYBILLukKm1A9Iy6fzMU45w6CkSsiGMZlA9j";
	public static String API_KEY = "RQuqNCGKJidfQLYvVME8OqcPkjjsgdXvYq0RN7YE";

	// connect to a URL and retrieve result
	// result is expected to be valid JSON
	public static JSONObject jsonFromUrl(HttpUriRequest request)
	{
		JSONObject result  = null;

		// server expects app keys in request headers
		request.addHeader("X-Parse-Application-Id", APP_ID);
		request.addHeader("X-Parse-REST-API-Key", API_KEY);

		HttpClient httpclient = new DefaultHttpClient();
		try
		{
			// send request and read response bytes
			HttpResponse response = httpclient.execute(request);

			ByteArrayOutputStream out = new ByteArrayOutputStream();
			response.getEntity().writeTo(out);
			out.close();

			// convert esult to json object
			String responseString = out.toString();
			result = new JSONObject(responseString);
		}
		catch (Exception ex)
		{
			Log.e(TodoAPI.class.toString(), ex.getMessage());
			ex.printStackTrace();
		}

		return result;
	}
}
