package trial.toptal.todo.network;

import android.view.ViewDebug;

import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.ByteArrayEntity;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 This class provides the following Item APIs:
 - get all items for a given user
 - sync existing items with server
 */

public class ItemAPI
{
	// parameter names for REST API
	public static String FIELD_OWNER = "owner";
	public static String FIELD_RESULT = "results";
	public static String FIELD_DUE = "dueDate";
	public static String FIELD_DONE = "isComplete";
	public static String FIELD_PRIORITY = "priority";
	public static String FIELD_TITLE = "title";
	public static String FIELD_ID = "intID";

	// parameter names for REST API (batch operations)
	public static String FIELD_PATH = "path";
	public static String FIELD_METHOD = "method";
	public static String FIELD_BODY = "body";

	// urls for REST API
	public static String ITEMS_URL = "https://api.parse.com/1/classes/Item";
	public static String SYNC_URL = "https://api.parse.com/1/batch";

	// this functions consumes the API at
	// https://api.parse.com/1/classes/Item
	// with added query constraints for user ID
	public static JSONObject getAllItems(String userID)
	{
		//add query constraint to url
		String query;
		try
		{
			query = String.format("where={\"%s\":\"%s\"}", FIELD_OWNER, userID);
			query = URLEncoder.encode(query, "UTF-8");
		}
		catch (UnsupportedEncodingException e)
		{
			// this should never happen
			e.printStackTrace();
			return null;
		}

		String url = String.format("%s?%s", ITEMS_URL, query);

		// query API is expecting a GET request
		HttpGet get = new HttpGet(url);
		return TodoAPI.jsonFromUrl(get);
	}

	// this function determines the operation needed for each item
	// and calls the corresponding API
	public static JSONObject syncData(JSONObject obj, String url, String method)
	{
		// choose approperiate http method object
		HttpUriRequest request;
		if (method.equalsIgnoreCase("POST"))
		{
			request = new HttpPost(url);
			((HttpPost)request).setEntity(new ByteArrayEntity(obj.toString().getBytes()));
		}
		else if (method.equalsIgnoreCase("PUT"))
		{
			request = new HttpPut(url);
			((HttpPut)request).setEntity(new ByteArrayEntity(obj.toString().getBytes()));
		}
		else if (method.equalsIgnoreCase("DELETE"))
		{
			request = new HttpDelete(url);
		}
		else
			return new JSONObject();

		return TodoAPI.jsonFromUrl(request);
	}

}
