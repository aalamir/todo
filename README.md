# About this app #

A simple Todo list app, using Parse.com for its backend, and demonstrates the Android rest architecture proposed by Android engineer Virgil Dobjanschi [in Google I/O](https://youtu.be/xHXn3Kg2IQE)

### How do I get set up? ###

* Clone/download code.
* Open in Android Studio.
* That's it.

### Notes ###
* The app is meant for demonstration only, access to backend has been disabled.