package trial.toptal.todo.provider;

/*
	Contract class for Todo Content Provider.
	This class provides defintions for:
	- Authority and top-level URI for the Content Provider
	- Column names, URIs and MIME types for data exposed by Content Provider
*/

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;

import trial.toptal.todo.database.ItemTable;
import trial.toptal.todo.database.UserTable;

public final class TodoContract
{
	// Authority
	public static final String AUTHORITY = "trial.toptal.todo.provider";
	public static final String MAIN_URI = "content://" + AUTHORITY;

	// Constants for User table
	public static final class User
	{
		// MIME type for a directory
		public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/todo.user";

		// MIME type for an item
		public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/todo.user";

		// Projection for all columns
		public static final String[] PROJECTION_ALL =
		{
				UserTable.COLUMN_ID,
				UserTable.COLUMN_UNAME,
				UserTable.COLUMN_NAME,
				UserTable.COLUMN_TOKEN,
				ItemTable.COLUMN_STATUS,
				ItemTable.COLUMN_RESULT
		};
	}

	// Constants for Item table
	public static final class Item
	{
		// MIME type for a directory
		public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/todo.item";

		// MIME type for an item
		public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/todo.item";

		// Projection for all columns
		public static final String[] PROJECTION_ALL =
		{
				ItemTable.COLUMN_ID,
				ItemTable.COLUMN_TITLE,
				ItemTable.COLUMN_DESC,
				ItemTable.COLUMN_PRIORITY,
				ItemTable.COLUMN_DUE,
				ItemTable.COLUMN_DONE,
				ItemTable.COLUMN_CREATED,
				ItemTable.COLUMN_MODIFIED,
				ItemTable.COLUMN_STATUS,
				ItemTable.COLUMN_RESULT,
				ItemTable.COLUMN_STR_ID
		};
	}
}


