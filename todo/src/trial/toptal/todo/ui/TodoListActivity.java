package trial.toptal.todo.ui;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CursorAdapter;

import android.text.format.DateFormat;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Date;

import de.timroes.android.listview.EnhancedListView;
import de.timroes.android.listview.EnhancedListView.*;
import trial.toptal.todo.R;
import trial.toptal.todo.business.Processor;
import trial.toptal.todo.database.ItemTable;
import trial.toptal.todo.database.Table;
import trial.toptal.todo.provider.TodoContract;
import trial.toptal.todo.services.SyncService;
import trial.toptal.todo.services.UserService;


public class TodoListActivity extends Activity
{
	/*
		EnhancedListView adds the following abilities to a native ListView:
		- Swipe to dismiss
		- Undo delete
	*/
	private EnhancedListView mListItems;

	// business logic
	private Processor mProcessor;

	// used for item editing activity
	private final int TODO_ITEM_REQUEST = 1008;

	// current user ID
	private String mUserID;

	// UI component
	private Button mBtnSync;

	// broadcast receiver for SyncService
	private BroadcastReceiver mReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			if(intent.getAction().equals(SyncService.SYNC_SERVICE_ACTION))
			{
				((CursorAdapter)mListItems.getAdapter()).getCursor().requery();
				enableSyncButton();
			}
		}
	};

	// handler for list item check button clicks
	TodoAdapter.CheckboxHandler mCheckHandler = new TodoAdapter.CheckboxHandler()
	{
		@Override
		public void onCheckboxClick(boolean newState, int itemID)
		{
			mProcessor.markItemAsDone(itemID, newState);
		}
	};

	// handler for list item edit button clicks
	TodoAdapter.EditHandler mEditHandler = new TodoAdapter.EditHandler()
	{
		@Override
		public void onEditClick(int pos)
		{
			Intent intent = new Intent(TodoListActivity.this, TodoItemActivity.class);
			Cursor cur = ((CursorAdapter)mListItems.getAdapter()).getCursor();

			// get column ids
			int idIndex = cur.getColumnIndex(ItemTable.COLUMN_ID);
			int titleIndex = cur.getColumnIndex(ItemTable.COLUMN_TITLE);
			int dueIndex = cur.getColumnIndex(ItemTable.COLUMN_DUE);
			int doneIndex = cur.getColumnIndex(ItemTable.COLUMN_DONE);
			int priorityIndex = cur.getColumnIndex(ItemTable.COLUMN_PRIORITY);

			// set title, due date and isDone vlaues
			cur.moveToPosition(pos);
			intent.putExtra(ItemTable.COLUMN_ID, cur.getInt(idIndex));
			intent.putExtra(ItemTable.COLUMN_TITLE, cur.getString(titleIndex));
			intent.putExtra(ItemTable.COLUMN_DUE, cur.getString(dueIndex));
			intent.putExtra(ItemTable.COLUMN_DONE, cur.getInt(doneIndex) == 1);
			intent.putExtra(ItemTable.COLUMN_PRIORITY, cur.getInt(priorityIndex));

			startActivityForResult(intent, TODO_ITEM_REQUEST);
		}
	};

	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_todo_list);

		mBtnSync = (Button)findViewById(R.id.btnSync);

		// extract user ID
		Bundle bundle = getIntent().getExtras();
		if (bundle == null || (mUserID = bundle.getString(WelcomeActivity.USER_ID)) == null)
		{
			Log.e(TodoListActivity.class.toString(), "Invalid arguments passed");
			finish();
		}

		// initialize business logic
		mProcessor = new Processor(this);

		// MULTILEVEL_UNDO allows serial undos of all removed items
		mListItems = (EnhancedListView)findViewById(R.id.lstItems);
		mListItems.setUndoStyle(UndoStyle.MULTILEVEL_POPUP);

		// set item dismiss callback. this function is called when
		// an item is removed
		mListItems.setDismissCallback(new OnDismissCallback()
		{
			@Override
			public Undoable onDismiss(EnhancedListView listView, int position)
			{
				// mark removed list item for deletion
				Cursor cur = ((CursorAdapter)mListItems.getAdapter()).getCursor();
				final Uri markedItem = mProcessor.markItemForDeletion(cur, position);

				// return an Undoable
				return new EnhancedListView.Undoable()
				{
					// in case users chooses to Undo, remove deletion mark from item
					@Override public void undo()
					{
						mProcessor.unmarkItemForDeletion(markedItem);
					}

					// in case users wants to actually delete it, do nothing and leave
					// it marked until next sync
					@Override public void discard()
					{
						// item will be actually delete from content provider on next sync
					}
				};
			}
		});

		// enable swipe to dimiss
		mListItems.setUndoHideDelay(3 * 1000);
		mListItems.enableSwipeToDismiss();

		// check if first run and get data from server
		startSync();
	}

	@Override
	protected void onResume()
	{
		super.onResume();

		loadItems(null);

		// setup broadcast listener for SyncService
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(SyncService.SYNC_SERVICE_ACTION);
		registerReceiver(mReceiver, intentFilter);

		// disable sync button if SyncService is running
		if (SyncService.mIsRunning)
			disableSyncButton();
		else
			enableSyncButton();
	}

	private void disableSyncButton()
	{
		mBtnSync.setText(R.string.button_syncing);
		mBtnSync.setEnabled(false);
	}

	private void enableSyncButton()
	{
		mBtnSync.setText(R.string.button_sync);
		mBtnSync.setEnabled(true);
	}

	private void loadItems(String order)
	{
		// make sure existing cursor is closed
		TodoAdapter adapter = (TodoAdapter)mListItems.getAdapter();
		if (adapter != null)
			adapter.changeCursor(null);

		Cursor cur = mProcessor.getAllItems(order);
		mListItems.setAdapter(new TodoAdapter(this, cur, mCheckHandler, mEditHandler));
	}

	@Override
	protected void onPause()
	{
		super.onPause();

		// transient data used for undo are lost by the reconstruction of
		// the activity
		mListItems.discardUndo();

		// make sure cursor is closed
		((CursorAdapter)mListItems.getAdapter()).changeCursor(null);

		// remove broadcast receiver
		try
		{
			unregisterReceiver(mReceiver);
		}
		catch (IllegalArgumentException ex)
		{
			// receiver was not registered, ignore
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		if (requestCode == TODO_ITEM_REQUEST && resultCode == RESULT_OK)
		{
			if (data == null)
				return;

			ContentValues values = new ContentValues();

			try
			{
				int id = data.getIntExtra(ItemTable.COLUMN_ID, -1);

				values.put(ItemTable.COLUMN_TITLE, data.getStringExtra(ItemTable.COLUMN_TITLE));
				values.put(ItemTable.COLUMN_DUE, data.getStringExtra(ItemTable.COLUMN_DUE));
				values.put(ItemTable.COLUMN_DONE, data.getBooleanExtra(ItemTable.COLUMN_DONE, false));
				values.put(ItemTable.COLUMN_PRIORITY, data.getIntExtra(ItemTable.COLUMN_PRIORITY, 0));

				if (id == -1)
				{
					// this is a new item
					mProcessor.addItem(values);
				}
				else
				{
					// modify existing item
					mProcessor.modifyItem(id, values);
				}
			}
			catch (NullPointerException ex)
			{
				Log.e(TodoListActivity.class.toString(), "Empty data passed to activity");
				ex.printStackTrace();
			}
		}
	}

	// this function is registered in XML layout files as the handler
	// for buttons clicking events
	public void onClick(View v)
	{
		if (v.getId() == R.id.btnSync)
		{
			startSync();
		}
	}

	// launch SyncService
	private void startSync()
	{
		if (SyncService.mIsRunning == false)
		{
			Intent intent = new Intent(this, SyncService.class);
			intent.putExtra(WelcomeActivity.USER_ID, mUserID);
			startService(intent);
		}

		// disable button while service is running
		disableSyncButton();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		int selectedItem = item.getItemId();
		if (selectedItem == R.id.mnuAdd)
		{
			Intent intent = new Intent(TodoListActivity.this, TodoItemActivity.class);
			startActivityForResult(intent, TODO_ITEM_REQUEST);
		}
		else if (selectedItem == R.id.mnuSortDate)
		{
			loadItems(ItemTable.COLUMN_DUE);
		}
		else if (selectedItem == R.id.mnuSortPriority)
		{
			loadItems(ItemTable.COLUMN_PRIORITY + " DESC");
		}
		else if (selectedItem == R.id.mnuLogout)
		{
			if (SyncService.mIsRunning)
			{
				Toast.makeText(TodoListActivity.this, R.string.error_logout, Toast.LENGTH_SHORT).show();
			}
			else
			{
				// remove all data and go to WelcomeActivity
				SharedPreferences prefs = this.getSharedPreferences("trial.toptal.todo", MODE_PRIVATE);
				SharedPreferences.Editor editor = prefs.edit();
				editor.clear();
				editor.commit();

				Uri uri = Uri.parse(TodoContract.MAIN_URI + "/item");
				getContentResolver().delete(uri, null, null);

				Intent intent = new Intent(TodoListActivity.this, WelcomeActivity.class);
				startActivity(intent);
				finish();
			}
		}
		return true;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.toto_list_menu, menu);

		return true;
	}

	// upon any change, a sync operation is automatically scheduled
	// to run 3 minutes after modification
	private void scheduleSync()
	{
		Intent intent = new Intent(TodoListActivity.this, SyncService.class);
		PendingIntent pintent = PendingIntent.getService(TodoListActivity.this, 0, intent, 0);

		AlarmManager alarm = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
		alarm.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + 30 * 1000, pintent);
	}
}