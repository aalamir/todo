package trial.toptal.todo.business;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.database.Cursor;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;
import java.util.Map.*;
import java.util.Set;

import trial.toptal.todo.database.ItemTable;
import trial.toptal.todo.database.Table;
import trial.toptal.todo.network.ItemAPI;

/**
	This class is responsible for handling data conversion between client
    readable (mostly database rows and ContentValues) and server readable
    format (JSON)
*/

public class Formatter
{
	// convert a JSON item from Server into a ContentValue suitable
	// for Content Provider
	public static ContentValues unpackItem(JSONObject item)
	{
		// extract values
		ContentValues values = new ContentValues();
		try
		{
			values.put(ItemTable.COLUMN_DUE, item.getString(ItemAPI.FIELD_DUE));
			values.put(ItemTable.COLUMN_DONE, item.getBoolean(ItemAPI.FIELD_DONE) ? 1 : 0);
			values.put(ItemTable.COLUMN_PRIORITY, item.getInt(ItemAPI.FIELD_PRIORITY));
			values.put(ItemTable.COLUMN_TITLE, item.getString(ItemAPI.FIELD_TITLE));
			values.put(ItemTable.COLUMN_ID, item.getInt(ItemAPI.FIELD_ID));
		}
		catch (JSONException ex)
		{
			Log.e(Formatter.class.toString(), "Could not upack item");
			ex.printStackTrace();
		}

		return values;
	}

	// convert current item in cursor to a suitable JSON for REST API
	// according to its status
	public static JSONObject packItem(Cursor cur, String owner)
	{
		JSONObject result = new JSONObject();

		try
		{
			// get column ids
			int idIndex = cur.getColumnIndex(ItemTable.COLUMN_STR_ID);
			int titleIndex = cur.getColumnIndex(ItemTable.COLUMN_TITLE);
			int dueIndex = cur.getColumnIndex(ItemTable.COLUMN_DUE);
			int doneIndex = cur.getColumnIndex(ItemTable.COLUMN_DONE);
			int priorityIndex = cur.getColumnIndex(ItemTable.COLUMN_PRIORITY);
			int statusIndex = cur.getColumnIndex(ItemTable.COLUMN_STATUS);

			String status = cur.getString(statusIndex);
			if (status.equalsIgnoreCase(Table.STATE_DELETING))
			{
				// for deletion, only the ID and method are needed
				result.put(ItemAPI.FIELD_PATH, ItemAPI.ITEMS_URL + "/" + cur.getString(idIndex));
				result.put(ItemAPI.FIELD_METHOD, "DELETE");
				result.put(ItemAPI.FIELD_BODY, new JSONObject());
			}
			else
			{
				// for other operations, the request type must be set
				if (status.equalsIgnoreCase(Table.STATE_INSERTING))
				{
					result.put(ItemAPI.FIELD_PATH, ItemAPI.ITEMS_URL);
					result.put(ItemAPI.FIELD_METHOD, "POST");
				}
				else if (status.equalsIgnoreCase(Table.STATE_UPDATING))
				{
					result.put(ItemAPI.FIELD_PATH, ItemAPI.ITEMS_URL + "/" + cur.getString(idIndex));
					result.put(ItemAPI.FIELD_METHOD, "PUT");
				}

				// and the actual data
				JSONObject body = new JSONObject();
				body.put(ItemAPI.FIELD_DUE, cur.getString(dueIndex));
				body.put(ItemAPI.FIELD_DONE, cur.getInt(doneIndex) == 1);
				body.put(ItemAPI.FIELD_PRIORITY, cur.getInt(priorityIndex));
				body.put(ItemAPI.FIELD_TITLE, cur.getString(titleIndex));
				body.put(ItemAPI.FIELD_OWNER, owner);

				result.put(ItemAPI.FIELD_BODY, body);
			}
		}
		catch (JSONException ex)
		{
			Log.e(Formatter.class.toString(), "Could not pack items");
			ex.printStackTrace();
		}

		return result;
	}

	// get itemID from cursor
	public static int getItemID(Cursor cur)
	{
		int idIndex = cur.getColumnIndex(ItemTable.COLUMN_ID);
		return cur.getInt(idIndex);
	}
}
