package trial.toptal.todo.provider;

/*
	This Content Provider is responsible for handling CRUD operations
	on the Todo database.
*/

import android.app.ActionBar;
import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;

import trial.toptal.todo.database.ItemTable;
import trial.toptal.todo.database.Table;
import trial.toptal.todo.database.TodoDBHelper;
import trial.toptal.todo.database.UserTable;

public class TodoProvider extends ContentProvider
{
	// The data source is the todo sqlite database
	TodoDBHelper mHelper;

	// helper constants for use with the UriMatcher
	private static final int USER_LIST = 1;
	private static final int USER_ID = 2;
	private static final int ITEM_LIST = 5;
	private static final int ITEM_ID = 6;

	private static final UriMatcher URI_MATCHER;

	// prepare the UriMatcher
	static
	{
		URI_MATCHER = new UriMatcher(UriMatcher.NO_MATCH);
		URI_MATCHER.addURI(TodoContract.AUTHORITY, "item", ITEM_LIST);
		URI_MATCHER.addURI(TodoContract.AUTHORITY, "item/#", ITEM_ID);
		URI_MATCHER.addURI(TodoContract.AUTHORITY, "user", USER_LIST);
		URI_MATCHER.addURI(TodoContract.AUTHORITY, "user/#", USER_ID);
	}

	@Override
	public boolean onCreate()
	{
		mHelper = new TodoDBHelper(getContext());
		return true;
	}

	@Override
	public String getType(Uri uri)
	{
		switch (URI_MATCHER.match(uri))
		{
			case ITEM_LIST:
				return TodoContract.Item.CONTENT_TYPE;
			case ITEM_ID:
				return TodoContract.Item.CONTENT_ITEM_TYPE;
			case USER_LIST:
				return TodoContract.User.CONTENT_TYPE;
			case USER_ID:
			return TodoContract.User.CONTENT_ITEM_TYPE;
			default:
				throw new IllegalArgumentException("Unsupported URI: " + uri);
		}
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder)
	{
		SQLiteDatabase db = mHelper.getReadableDatabase();
		SQLiteQueryBuilder builder = new SQLiteQueryBuilder();

		switch (URI_MATCHER.match(uri))
		{
			case ITEM_LIST:
			{
				// retrieve all items. if no order query is specified, use the default
				builder.setTables(ItemTable.TABLE_NAME);
				if (TextUtils.isEmpty(sortOrder))
					sortOrder = ItemTable.SORT_COLUMN;

				break;
			}
			case ITEM_ID:
			{
				// retrieve a certain item
				builder.setTables(ItemTable.TABLE_NAME);
				builder.appendWhere(ItemTable.COLUMN_ID + " = " + uri.getLastPathSegment());
				break;
			}
			case USER_LIST:
			{
				// retrieve all users. if no order query is specified, use the default
				builder.setTables(UserTable.TABLE_NAME);
				if (TextUtils.isEmpty(sortOrder))
					sortOrder = UserTable.SORT_COLUMN;

				break;
			}
			case USER_ID:
			{
				// retrieve a certain user
				builder.setTables(UserTable.TABLE_NAME);
				builder.appendWhere(UserTable.COLUMN_ID + " = " + uri.getLastPathSegment());
				break;
			}
			default:
				throw new IllegalArgumentException("Unsupported URI: " + uri);
		}

		Cursor cursor = builder.query(db, projection, selection, selectionArgs, null, null, sortOrder);
		return cursor;
	}

	@Override
	public Uri insert(Uri uri, ContentValues values)
	{
		String tableName = null;

		// make sure the passed Uri is of directory type for items or users
		// and select corresponding table name
		if (URI_MATCHER.match(uri) == ITEM_LIST)
			tableName = ItemTable.TABLE_NAME;
		else if (URI_MATCHER.match(uri) != USER_LIST)
			tableName = UserTable.TABLE_NAME;
		else
			throw new IllegalArgumentException("Unsupported URI for insertion: " + uri);

		SQLiteDatabase db = mHelper.getWritableDatabase();

		long id = db.insert(tableName, null, values);

		// notify listeners that a change has occurred
		Uri result = ContentUris.withAppendedId(uri, id);
		if (id > 0)
			getContext().getContentResolver().notifyChange(result, null);

		return result;
	}

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs)
	{
		SQLiteDatabase db = mHelper.getWritableDatabase();
		int delCount = 0;

		switch (URI_MATCHER.match(uri))
		{
			case ITEM_LIST:
			{
				// delete items dir, according to selection
				delCount = db.delete(ItemTable.TABLE_NAME, selection, selectionArgs);
				break;
			}
			case ITEM_ID:
			{
				// delete a certain item, given its id
				String idStr = uri.getLastPathSegment();
				String where = ItemTable.COLUMN_ID + " = " + idStr;
				if (!TextUtils.isEmpty(selection))
					where += " AND " + selection;

				delCount = db.delete(ItemTable.TABLE_NAME, where, selectionArgs);
				break;
			}
			case USER_LIST:
			{
				// delete users dir, according to values
				delCount = db.delete(UserTable.TABLE_NAME, selection, selectionArgs);
				break;
			}
			case USER_ID:
			{
				// delete a certain user, given its id
				String idStr = uri.getLastPathSegment();
				String where = UserTable.COLUMN_ID + " = " + idStr;
				if (!TextUtils.isEmpty(selection))
					where += " AND " + selection;

				delCount = db.delete(UserTable.TABLE_NAME, where, selectionArgs);
				break;
			}
			default:
				throw new IllegalArgumentException("Unsupported URI: " + uri);
		}

		// notify all listeners of changes:
		if (delCount > 0)
			getContext().getContentResolver().notifyChange(uri, null);

		return delCount;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs)
	{
		SQLiteDatabase db = mHelper.getWritableDatabase();
		int updateCount = 0;

		switch (URI_MATCHER.match(uri))
		{
			case ITEM_LIST:
			{
				// update items dir, according to values
				updateCount = db.update(ItemTable.TABLE_NAME, values, selection, selectionArgs);
				break;
			}
			case ITEM_ID:
			{
				// update a certain item, given its id
				String idStr = uri.getLastPathSegment();
				String where = ItemTable.COLUMN_ID + " = " + idStr;
				if (!TextUtils.isEmpty(selection))
					where += " AND " + selection;

				updateCount = db.update(ItemTable.TABLE_NAME, values, where, selectionArgs);
				break;
			}
			case USER_LIST:
			{
				// update users dir, according to values
				updateCount = db.update(UserTable.TABLE_NAME, values, selection, selectionArgs);
				break;
			}
			case USER_ID:
			{
				// update a certain user, given its id
				String idStr = uri.getLastPathSegment();
				String where = UserTable.COLUMN_ID + " = " + idStr;
				if (!TextUtils.isEmpty(selection))
					where += " AND " + selection;

				updateCount = db.update(UserTable.TABLE_NAME, values, where, selectionArgs);
				break;
			}
			default:
				throw new IllegalArgumentException("Unsupported URI: " + uri);
		}

		// notify all listeners of changes:
		if (updateCount > 0)
			getContext().getContentResolver().notifyChange(uri, null);

		return updateCount;
	}
}
