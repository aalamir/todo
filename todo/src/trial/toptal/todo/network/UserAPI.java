package trial.toptal.todo.network;

import android.util.Log;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

/**
	This class provides the following User APIs:
    - sign up
    - sign in
*/

public class UserAPI
{
	// parameter names for REST API
	public static String FIELD_USERNAME = "username";
	public static String FIELD_PASSWORD = "password";

	// urls for REST API
	public static String SIGNUP_URL = "https://api.parse.com/1/users";
	public static String SIGNIN_URL = "https://api.parse.com/1/login";

	public static JSONObject signUp(String userName, String password)
	{
		// convert user data to JSON
		JSONObject user = new JSONObject();
		try
		{
			user.put(UserAPI.FIELD_USERNAME, userName);
			user.put(UserAPI.FIELD_PASSWORD, password);
		}
		catch (JSONException e)
		{
			Log.e(UserAPI.class.toString(), "Could not convert user info to JSON");
			e.printStackTrace();

			return null;
		}

		// user signup expects a POST request
		HttpPost post = new HttpPost(SIGNUP_URL);
		post.setEntity(new ByteArrayEntity(user.toString().getBytes()));

		return TodoAPI.jsonFromUrl(post);
	}

	public static JSONObject signIn(String userName, String password)
	{
		// setup paramters
		String url;
		try
		{
			String encodedName, encodedPass;
			encodedName = URLEncoder.encode(userName, "UTF-8");
			encodedPass = URLEncoder.encode(password, "UTF-8");

			url = String.format("%s?%s=%s&%s=%s", SIGNIN_URL, FIELD_USERNAME, encodedName, FIELD_PASSWORD, encodedPass);
		}
		catch (UnsupportedEncodingException e)
		{
			// this should never happen
			e.printStackTrace();
			return null;
		}

		// user signup expects a GET request
		HttpGet get = new HttpGet(url);
		return TodoAPI.jsonFromUrl(get);
	}
}
