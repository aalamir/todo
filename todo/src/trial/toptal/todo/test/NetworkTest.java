package trial.toptal.todo.test;


import junit.framework.Assert;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Random;

import trial.toptal.todo.network.ItemAPI;
import trial.toptal.todo.network.TodoAPI;
import trial.toptal.todo.network.UserAPI;

public class NetworkTest
{
	public static void testTodoAPI()
	{
		/*
			Test case: https://www.parse.com/docs/rest#cloudfunctions
			Purpose: check if TodoAPI.jsonFromUrl is able to access given url
					 and retrieve expected data

			mehotd: POST

			expected result:
			{
				"code": 141,
				"error": "function not found"
			}
		*/

		String url = "https://api.parse.com/1/functions/hello";
		HttpPost post = new HttpPost(url);

		JSONObject obj = TodoAPI.jsonFromUrl(post);
		Assert.assertTrue(obj.has("code"));
		Assert.assertTrue(obj.has("error"));
	}

	private static String random(int len)
	{
		String chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
		Random random = new Random(System.currentTimeMillis());

		StringBuffer randStr = new StringBuffer();
		for (int i = 0; i < len; ++i)
		{
			int number = random.nextInt(chars.length());
			char ch = chars.charAt(number);
			randStr.append(ch);
		}
		return randStr.toString();
	}

	public static void testUserSignup()
	{
		/*
			Test case: https://www.parse.com/docs/rest#users-signup
			Depends on: TodoAPI.jsonFromUrl
			Purpose: check if UserAPI.signUp delivers data correctly
					 and retrieves expected data

			mehotd: POST

			expected result:
			{
				"createdAt": "2011-11-07T20:58:34.448Z",
				"objectId": "g7y9tkhB7O",
				"sessionToken": "pnktnjyb996sj4p156gjtp4im"
			}
		*/

		JSONObject obj = UserAPI.signUp(random(8), random(8));

		Assert.assertTrue(obj.has("createdAt"));
		Assert.assertTrue(obj.has("sessionToken"));
		Assert.assertTrue(obj.has("objectId"));
	}

	public static void testUserSignin()
	{
		/*
			Test case: https://www.parse.com/docs/rest#users-login
			Depends on: TodoAPI.jsonFromUrl
			Purpose: check if UserAPI.signIn delivers data correctly
					 and retrieves expected data

			mehotd: GET

			expected result:
			{
				"code": 101,
				"error": "invalid login parameters"
			}
		*/

		JSONObject obj = UserAPI.signIn(random(8), random(8));

		Assert.assertTrue(obj.has("code"));
		Assert.assertTrue(obj.has("error"));
	}

	public static void testItemGetAll()
	{
		/*
			Test case: https://www.parse.com/docs/rest#queries-constraints
			Depends on: TodoAPI.jsonFromUrl
			Purpose: check if ItemAPI.getAllItems delivers data correctly
					 and retrieves expected data

			mehotd: GET

			expected result:
			{
				"results": []
			}
		*/

		JSONObject obj = ItemAPI.getAllItems("2PfIGf1ZVa");

		Assert.assertTrue(obj.has(ItemAPI.FIELD_RESULT));
	}

	public static void testSyncData()
	{
		/*
			Test case: https://www.parse.com/docs/rest#objects
			Depends on: TodoAPI.jsonFromUrl
			Purpose: check if ItemAPI.syncData delivers data correctly
					 and retrieves expected data

			mehotd: POST-PUT-DELETE

			procedure: create and object, modify it, then delete it

			expected result 1:
			{
				"createdAt": "2011-08-20T02:06:57.931Z",
				"objectId": "Ed1nuqPvcm"
			}

			expected result 2:
			{
				"updatedAt": "2011-08-21T18:02:52.248Z"
			}

			expected result 3:

		*/

		String testObj = "{\"dueDate\":\"2014-5-30\"}";
		String url = "https://api.parse.com/1/classes/Item";

		try
		{
			// test 1
			JSONObject result = ItemAPI.syncData(new JSONObject(testObj), url, "POST");
			Assert.assertTrue(result.has("createdAt"));
			Assert.assertTrue(result.has("objectId"));

			// test 2
			String itemID = result.getString("objectId");
			result = ItemAPI.syncData(new JSONObject(testObj), url + "/" + itemID, "PUT");
			Assert.assertTrue(result.has("updatedAt"));

			// test 3
			result = ItemAPI.syncData(new JSONObject(testObj), url + "/" + itemID, "DELETE");
			Assert.assertFalse(result.has("error"));

		}
		catch (JSONException ex)
		{
			// ignore, test string is guaranteed
			// other cases are being asserted
			ex.printStackTrace();
		}
	}
}
