package trial.toptal.todo.business;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.text.format.DateFormat;

import java.util.Date;
import java.util.HashMap;

import trial.toptal.todo.database.ItemTable;
import trial.toptal.todo.database.Table;
import trial.toptal.todo.provider.TodoContract;

/*
	This class is responsible for implementing the business logic of the
	application. It serves as an intermediate layer between UI and content
	provider:

		- manipulates data states
		- handles syncing requests
*/
public class Processor
{
	private Context mContext;

	// this map stores the status of a row before it was marked for deletion
	// to be able to restore it in case of deletion undo
	private HashMap<Integer, String> mStateBeforeDelete = new HashMap<>();

	public Processor(Context c)
	{
		mContext = c;
	}

	// Marks a certain item for deletion
	// Returns its Uri
	public Uri markItemForDeletion(Cursor cur, int pos)
	{
		// retrieve the index of the ID and Status columns
		int idIndex = cur.getColumnIndex(ItemTable.COLUMN_ID);
		int stateIndex = cur.getColumnIndex(ItemTable.COLUMN_STATUS);
		cur.moveToPosition(pos);

		// retrieve ID and state using column index
		String status  = cur.getString(stateIndex);
		int itemID = cur.getInt(idIndex);
		Uri uri = Uri.parse(TodoContract.MAIN_URI + "/item/" + String.valueOf(itemID));

		mStateBeforeDelete.put(itemID, status);

		// mark row for deletion
		ContentValues values = new ContentValues();
		values.put(Table.COLUMN_STATUS, Table.STATE_DELETING);

		mContext.getContentResolver().update(uri, values, null, null);

		// due to the way EnahncedListView animates removed items, we cannot
		// wait until Content Provider notifications take place, as this will
		// cause the removed item to falsely show in the list
		cur.requery();

		return uri;
	}

	// Unmarks deleted item, and returns it to its previous status
	public void unmarkItemForDeletion(Uri uri)
	{
		// try to retrieve any saved status for this row
		int id = Integer.parseInt(uri.getLastPathSegment());
		String prevStatus = Table.STATE_SYNCED;
		if (mStateBeforeDelete.containsKey(id))
		{
			prevStatus = mStateBeforeDelete.get(id);
			mStateBeforeDelete.remove(id);
		}

		ContentValues values = new ContentValues();
		values.put(Table.COLUMN_STATUS, prevStatus);

		mContext.getContentResolver().update(uri, values, null, null);
	}

	// retrieves all items from content provider excluding those who are
	// marked for deletion. closing the cursor is the caller's responsibility
	public Cursor getAllItems(String sort)
	{
		ContentResolver resolver = mContext.getContentResolver();

		Uri uri = Uri.parse(TodoContract.MAIN_URI + "/item");
		String where = ItemTable.COLUMN_STATUS + " != ?";
		String[] args = new String[] {Table.STATE_DELETING};

		Cursor cur = resolver.query(uri, TodoContract.Item.PROJECTION_ALL, where, args, sort);
		cur.setNotificationUri(resolver, uri);

		return cur;
	}

	// toggle the isDone state for an item
	public void markItemAsDone(int itemID, boolean isDone)
	{
		Uri uri = Uri.parse(TodoContract.MAIN_URI + "/item/" + String.valueOf(itemID));

		ContentValues values = new ContentValues();
		values.put(ItemTable.COLUMN_DONE, isDone ? 1 : 0);

		// check existing state, inserting will remain inserting
		// otherwise is converted to updating
		ContentResolver resolver = mContext.getContentResolver();
		Cursor cur = resolver.query(uri, new String[] {ItemTable.COLUMN_STATUS}, null, null, null);

		if (cur.moveToFirst() && cur.getString(0).equalsIgnoreCase(Table.STATE_INSERTING) == false)
			values.put(ItemTable.COLUMN_STATUS, Table.STATE_UPDATING);

		cur.close();
		resolver.update(uri, values, null, null);
	}

	// create a new item
	public Uri addItem(ContentValues values)
	{
		String now = DateFormat.format(Table.DATE_FORMAT, new Date()).toString();

		// set creation and modification dates to now
		values.put(ItemTable.COLUMN_CREATED, now);
		values.put(ItemTable.COLUMN_MODIFIED, now);

		// set status to inserting
		values.put(Table.COLUMN_STATUS, Table.STATE_INSERTING);

		return mContext.getContentResolver().insert(Uri.parse(TodoContract.MAIN_URI + "/item"), values);
	}

	// modify an existing item
	public boolean modifyItem(int itemID, ContentValues values)
	{
		Uri itemUri = Uri.parse(TodoContract.MAIN_URI + "/item/" + String.valueOf(itemID));

		// update modification date
		String now = DateFormat.format(Table.DATE_FORMAT, new Date()).toString();
		values.put(ItemTable.COLUMN_MODIFIED, now);

		// set status to updating
		values.put(Table.COLUMN_STATUS, Table.STATE_UPDATING);

		int numUpdated = mContext.getContentResolver().update(itemUri, values, null, null);
		return numUpdated == 1;
	}
}
