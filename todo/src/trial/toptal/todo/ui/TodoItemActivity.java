package trial.toptal.todo.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.NumberPicker;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import trial.toptal.todo.R;
import trial.toptal.todo.database.ItemTable;
import trial.toptal.todo.database.Table;

public class TodoItemActivity extends Activity
{
	private CheckBox mChkDone;
	private EditText mEdtTitle;
	private DatePicker mDatePicker;
	private NumberPicker mNumPicker;

	private int mItemID = -1;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_todo_item);

		mChkDone = (CheckBox)findViewById(R.id.chkDone);
		mEdtTitle = (EditText)findViewById(R.id.edtTitle);
		mDatePicker = (DatePicker)findViewById(R.id.datePicker);
		mNumPicker = (NumberPicker)findViewById(R.id.numberPicker);

		// setup boundaries
		mNumPicker.setMinValue(1);
		mNumPicker.setMaxValue(3);
	}

	@Override
	protected void onResume()
	{
		super.onResume();

		Bundle extras = getIntent().getExtras();
		if (extras == null)
			return;

		mItemID = extras.getInt(ItemTable.COLUMN_ID);
		mChkDone.setChecked(extras.getBoolean(ItemTable.COLUMN_DONE));
		mEdtTitle.setText(extras.getString(ItemTable.COLUMN_TITLE));
		mNumPicker.setValue(extras.getInt(ItemTable.COLUMN_PRIORITY) + 1);

		// extract date according to db format
		SimpleDateFormat sdf = new SimpleDateFormat(Table.DATE_FORMAT);
		try
		{
			Date date = sdf.parse(extras.getString(ItemTable.COLUMN_DUE));
			Calendar c = Calendar.getInstance();
			c.setTime(date);

			mDatePicker.updateDate(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
		}
		catch (ParseException e)
		{
			Log.e(TodoItemActivity.class.toString(), "Invalid date format passed to activity");
			e.printStackTrace();
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		int selectedItem = item.getItemId();
		if (selectedItem == R.id.mnuSave)
		{
			Intent intent = new Intent(TodoItemActivity.this, TodoListActivity.class);

			intent.putExtra(ItemTable.COLUMN_ID, mItemID);
			intent.putExtra(ItemTable.COLUMN_TITLE, mEdtTitle.getText().toString());
			intent.putExtra(ItemTable.COLUMN_DONE, mChkDone.isChecked());
			intent.putExtra(ItemTable.COLUMN_PRIORITY, mNumPicker.getValue() - 1);

			SimpleDateFormat sdf = new SimpleDateFormat(Table.DATE_FORMAT);
			Calendar c = Calendar.getInstance();
			c.set(mDatePicker.getYear(), mDatePicker.getMonth(), mDatePicker.getDayOfMonth());

			intent.putExtra(ItemTable.COLUMN_DUE, sdf.format(c.getTime()));

			setResult(RESULT_OK, intent);
			finish();
		}

		return true;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.todo_item_menu, menu);

		return true;
	}
}