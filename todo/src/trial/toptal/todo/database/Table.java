package trial.toptal.todo.database;

/*
	Table represents the a database table.
	This class is responsible for the table creation/upgrade via SQL queries.
*/

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public abstract class Table
{
	// A common column for all tables is the ID column
	public static final String COLUMN_ID = "_id";

	// Since all database operations need to be mirrored on the server, these values
	// keeps track of the state each row. When a db operation has been successfully
	// mirrored on server, the row state is set to STATE_SYNCED
	public static String STATE_INSERTING = "STATE_INSERTING";
	public static String STATE_UPDATING = "STATE_UPDATING";
	public static String STATE_DELETING = "STATE_DELETING";
	public static String STATE_SYNCED = "STATE_SYNCED";

	// dates are stored as strings, with the following format, taken from sqlite docs
	public static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

	// the columns Status and NetworkResult keep track of the transitional state
	// between syncs and result of network operations. this eases recovery from network faults
	public static final String COLUMN_STATUS = "Status";
	public static final String COLUMN_RESULT = "NetworkResult";

	private String mCreateQuery;    // Table creation SQL statement
	private String mTableName;      // Table name

	public Table(String name, String query)
	{
		mCreateQuery = query;
		mTableName = name;
	}
	public void onCreate(SQLiteDatabase database)
	{
		database.execSQL(mCreateQuery);
	}

	public void onUpgrade(SQLiteDatabase database, int oldVer, int newVer)
	{
		Log.w(ItemTable.class.getName(), String.format("Upgrading database from version %d to %d.", oldVer, newVer));
		database.execSQL("DROP TABLE IF EXISTS " + mTableName);
		onCreate(database);
	}
}
