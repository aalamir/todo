package trial.toptal.todo.ui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.regex.Pattern;

import trial.toptal.todo.R;
import trial.toptal.todo.services.UserService;
import trial.toptal.todo.test.NetworkTest;

public class WelcomeActivity extends Activity
{
	// Keys used in shared preferences
	public static String USER_EXISTS = "USER_EXISTS";
	public static String USER_TOKEN = "USER_TOKEN";
	public static String USER_ID = "USER_ID";

	// UI elements
	EditText mEdtUserName;
	EditText mEdtPassword;

	// broadcast receiver for UserService
	private BroadcastReceiver mReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			if(intent.getAction().equals(UserService.USER_SERVICE_ACTION))
			{
				if (mProgress.isShowing())
					mProgress.dismiss();

				if (intent.getBooleanExtra(UserService.SUCCESS_KEY, false))
				{
					String userID = intent.getStringExtra(USER_ID);
					loadTodoListActivity(userID);
				}
				else
				{
					String errorMsg = intent.getStringExtra(UserService.RESULT_MSG_KEY);
					if (errorMsg != null)
						Toast.makeText(WelcomeActivity.this, errorMsg, Toast.LENGTH_LONG).show();
				}
			}
		}
	};

	// for when waiting for UserSerice
	ProgressDialog mProgress;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		mEdtPassword = (EditText)findViewById(R.id.edtPassword);
		mEdtUserName = (EditText)findViewById(R.id.edtUserName);

		// setup progress dialog
		mProgress = new ProgressDialog(this);
		mProgress.setMessage("Contacting server");
		mProgress.setIndeterminate(true);
		mProgress.setCancelable(false);

		// for test only
		//new TestTask().execute();
	}

	// for test only
	private class TestTask extends AsyncTask<Void, Void, Void>
	{

		@Override
		protected Void doInBackground(Void... params)
		{
			NetworkTest.testTodoAPI();
			NetworkTest.testUserSignup();
			NetworkTest.testUserSignin();

			NetworkTest.testItemGetAll();
			NetworkTest.testSyncData();

			return null;
		}
	}

	// simple text validation
	private boolean validateInput()
	{
		/*
			alphanumeric
			starts with a letter
		 */

		Pattern validForm = Pattern.compile("^[a-zA-Z][a-zA-Z0-9]*");

		String userName = mEdtUserName.getText().toString();
		String password = mEdtPassword.getText().toString();

		if (validForm.matcher(userName).matches() == false || validForm.matcher(userName).matches() == false)
		{
			Toast.makeText(WelcomeActivity.this, R.string.error_input, Toast.LENGTH_SHORT).show();
			return false;
		}

		return true;
	}

	// this function is registered in XML layout files as the handler
	// for buttons clicking events
	public void onClick(View view)
	{
		int viewID = view.getId();
		if (viewID == R.id.btnSignin || viewID == R.id.btnSignup)
		{
			if (validateInput() == false)
				return;

			mProgress.show();

			String userName = mEdtUserName.getText().toString();
			String password = mEdtPassword.getText().toString();

			// send credential and requested operations to UserService
			// and wait for feedbacl
			Intent intent = new Intent(WelcomeActivity.this, UserService.class);
			intent.putExtra(UserService.USER_NAME_KEY, userName);
			intent.putExtra(UserService.PASSWORD_KEY, password);
			intent.putExtra(UserService.SIGNUP_KEY, viewID == R.id.btnSignup);

			listenForUserService();
			startService(intent);
		}
	}

	// check shared preferences for user status
	// returns userID if one exists, or null otherwise
	private String checkUserStatus()
	{
		SharedPreferences prefs = this.getSharedPreferences("trial.toptal.todo", MODE_PRIVATE);
		SharedPreferences.Editor editor = prefs.edit();

		String userID = null;
		boolean userExists = prefs.getBoolean(USER_EXISTS, false);
		if (userExists)
			userID = prefs.getString(USER_ID, null);

		return userID;
	}

	@Override
	protected void onResume()
	{
		super.onResume();

		String userID = checkUserStatus();
		if (userID == null)
		{
			// no user detected, check if a network operation is ongoing
			if (UserService.mIsRunning)
			{
				// show waiting dialog
				if (mProgress.isShowing() == false)
					mProgress.show();

				// wait for service to come back
				listenForUserService();
			}
		}
		else
		{
			// user exists, move along to todo list activity
			loadTodoListActivity(userID);
		}
	}

	private void listenForUserService()
	{
		// setup broadcast listener for UserService
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(UserService.USER_SERVICE_ACTION);
		registerReceiver(mReceiver, intentFilter);
	}

	// send user id to ToDoListActivity and finish
	private void loadTodoListActivity(String userID)
	{
		Intent intent = new Intent(WelcomeActivity.this, TodoListActivity.class);
		intent.putExtra(USER_ID, userID);
		startActivity(intent);

		finish();
	}

	@Override
	protected void onPause()
	{
		super.onPause();

		// remove waiting dialog if it is on
		if (mProgress.isShowing())
			mProgress.dismiss();

		// remove broadcast receiver
		try
		{
			unregisterReceiver(mReceiver);
		}
		catch (IllegalArgumentException ex)
		{
			// receiver was not registered, ignore
		}
	}
}
