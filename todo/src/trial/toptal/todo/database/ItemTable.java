package trial.toptal.todo.database;

/*
	ItemTable represents the database table Item.
	This class is responsible for the creation/upgrade of this table
	via SQL queries.
*/

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class ItemTable extends Table
{
	// Table name and columns
	public static final String TABLE_NAME = "Item";
	public static final String COLUMN_TITLE     = "Title";
	public static final String COLUMN_DESC      = "Description";
	public static final String COLUMN_PRIORITY  = "Priority";
	public static final String COLUMN_DUE       = "DueDate";
	public static final String COLUMN_DONE      = "IsDone";
	public static final String COLUMN_STR_ID    = "serverID";

	// creation and modification help track syncing process
	public static final String COLUMN_CREATED   = "CreatedAt";
	public static final String COLUMN_MODIFIED  = "ModifiedAt";

	// Database creation SQL statement
	private static final String TABLE_QUERY = "CREATE TABLE "
			+ TABLE_NAME
			+ " ("
			+ COLUMN_ID         + " INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ COLUMN_TITLE      + " TEXT NOT NULL, "
			+ COLUMN_DESC       + " TEXT, "
			+ COLUMN_PRIORITY   + " INTEGER NOT NULL, "
			+ COLUMN_DONE       + " INTEGER NOT NULL, "
			+ COLUMN_DUE        + " TEXT NOT NULL, "    //YYYY-MM-DD HH:MM:SS.SSS (refer to SQLite website)
			+ COLUMN_CREATED    + " TEXT, "             //YYYY-MM-DD HH:MM:SS.SSS (refer to SQLite website)
			+ COLUMN_MODIFIED   + " TEXT, "             //YYYY-MM-DD HH:MM:SS.SSS (refer to SQLite website)
			+ COLUMN_STATUS     + " TEXT NOT NULL, "
			+ COLUMN_RESULT     + " INTEGER NOT NULL DEFAULT 0,"
			+ COLUMN_STR_ID     + " TEXT"               // used for server-generated id values
			+ ");";

	// default order column
	public static final String SORT_COLUMN = COLUMN_DUE;

	public ItemTable()
	{
		super(TABLE_NAME, TABLE_QUERY);
	}
}
