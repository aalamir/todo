package trial.toptal.todo.services;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import trial.toptal.todo.network.UserAPI;
import trial.toptal.todo.ui.WelcomeActivity;

/*
	This service is started to signup/sign users and report back
	via an intent
*/
public class UserService extends Service
{
	// action string for intent listeners
	public static final String USER_SERVICE_ACTION  = "trial.toptal.todo.services.UserService";

	// keys for data passing
	public static final String USER_NAME_KEY = "USER_NAME";
	public static final String PASSWORD_KEY = "PASSWORD";
	public static final String SIGNUP_KEY = "SIGNUP";
	public static final String RESULT_MSG_KEY = "RESULT";
	public static final String SUCCESS_KEY = "SUCCESS";

	// service status
	public static boolean mIsRunning = false;
	
	public IBinder onBind(Intent intent)
	{
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId)
	{
		// return value: if the service is killed by system, leave it dead
		int returnValue = START_NOT_STICKY;

		// retrieve user name and password, as well as required operation
		Bundle extras = intent.getExtras();
		String userName = null;
		String password = null;
		Boolean isSignUp = null;

		if (extras != null)
		{
			userName = extras.getString(USER_NAME_KEY);
			password = extras.getString(PASSWORD_KEY);
			isSignUp = extras.getBoolean(SIGNUP_KEY);
		}

		if (userName == null || password == null || isSignUp == null)
		{
			Log.e(UserService.class.toString(), "Invalid arguments passed");
			stop();

			return returnValue;
		}

		// convert user info into a server readable format


		// issue network call
		String[] args = new String[] {userName, password};
		if (isSignUp)
			new UserSignUp().execute(args);
		else
			new UserSignIn().execute(args);

		mIsRunning = true;
		return returnValue;
	}

	// common function to be called upon signup/signin finish
	private void userOperationDone(JSONObject result)
	{
		/*
			failure JSON:
			{
				"code": XXX,    -> integer
				"error": YYY    -> string
			}

			success JSON:
			{
				"objectId": XXX,    -> string
				"sessionToken": YYY -> string
			}
		*/

		// intent to be sent to receivers
		Intent resultIntent = new Intent(USER_SERVICE_ACTION);

		if (result == null)
		{
			resultIntent.putExtra(SUCCESS_KEY, false);
			resultIntent.putExtra(RESULT_MSG_KEY, "Network Error");
		}
		else if (result.has("error"))
		{
			// request failed, report back with erro
			try
			{
				String errorMsg = result.getString("error");

				// set status in intent
				resultIntent.putExtra(SUCCESS_KEY, false);
				resultIntent.putExtra(RESULT_MSG_KEY, errorMsg);
			}
			catch (JSONException e)
			{
				// should never happen
				e.printStackTrace();
			}
		}
		else if (result.has("sessionToken"))
		{
			// request succeeded, save data and report back
			SharedPreferences prefs = this.getSharedPreferences("trial.toptal.todo", MODE_PRIVATE);
			SharedPreferences.Editor editor = prefs.edit();

			try
			{
				String userID = result.getString("objectId");
				String userToken = result.getString("sessionToken");

				// save data in preferecnces
				editor.putBoolean(WelcomeActivity.USER_EXISTS, true);
				editor.putString(WelcomeActivity.USER_ID, userID);
				editor.putString(WelcomeActivity.USER_TOKEN, userToken);
				editor.commit();

				// set status in intent
				resultIntent.putExtra(SUCCESS_KEY, true);
				resultIntent.putExtra(WelcomeActivity.USER_ID, userID);
			}
			catch (JSONException e)
			{
				// should never happen
				e.printStackTrace();
			}

		}

		// broadcast
		sendBroadcast(resultIntent);

		// we are done
		stop();
	}

	// set running status to false and exit
	private void stop()
	{
		mIsRunning = false;
		stopSelf();
	}

	// this AsyncTask calls passed UserAPI.signUp function
	// and broadcasts the result to intent receivers
	private class UserSignUp extends AsyncTask<String, Void, JSONObject>
	{

		@Override
		protected JSONObject doInBackground(String... data)
		{
			return UserAPI.signUp(data[0], data[1]);
		}

		@Override
		protected void onPostExecute(JSONObject jsonObj)
		{
			userOperationDone(jsonObj);
		}
	}

	// this AsyncTask calls passed UserAPI.signIn function
	// and broadcasts the result to intent receivers
	private class UserSignIn extends AsyncTask<String, Void, JSONObject>
	{

		@Override
		protected JSONObject doInBackground(String... data)
		{
			return UserAPI.signIn(data[0], data[1]);
		}

		@Override
		protected void onPostExecute(JSONObject jsonObj)
		{
			userOperationDone(jsonObj);
		}
	}
}
