package trial.toptal.todo.database;

/*
	ItemTable represents the database table User.
	This class is responsible for the creation/upgrade of this table
	via SQL queries.
*/

public class UserTable extends Table
{
	// Table name and columns
	public static final String TABLE_NAME = "User";
	public static final String COLUMN_UNAME     = "UserName";
	public static final String COLUMN_NAME      = "FullName";
	public static final String COLUMN_TOKEN     = "Token";

	public static final String TABLE_QUERY = "CREATE TABLE "
			+ TABLE_NAME
			+ " ("
			+ COLUMN_ID         + " INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ COLUMN_UNAME      + " TEXT NOT NULL, "
			+ COLUMN_NAME       + " TEXT NOT NULL, "
			+ COLUMN_TOKEN      + " TEXT, "
			+ COLUMN_STATUS     + " TEXT NOT NULL, "
			+ COLUMN_RESULT     + " INTEGER NOT NULL DEFAULT 0"
			+ ");";

	// default order column
	public static final String SORT_COLUMN = COLUMN_NAME;

	public UserTable()
	{
		super(TABLE_NAME, TABLE_QUERY);
	}
}
