package trial.toptal.todo.services;

import android.app.Service;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import trial.toptal.todo.business.Formatter;
import trial.toptal.todo.database.ItemTable;
import trial.toptal.todo.database.Table;
import trial.toptal.todo.network.ItemAPI;
import trial.toptal.todo.provider.TodoContract;
import trial.toptal.todo.ui.WelcomeActivity;

/*
	This service is started to sync local data with server. changes are
	reflected in content provider which notifies its listeners.
*/
public class SyncService extends Service
{
	// action string for intent listeners
	public static final String SYNC_SERVICE_ACTION  = "trial.toptal.todo.services.SyncService";

	// current logged in user
	private String mUserID;

	// service status
	public static boolean mIsRunning = false;

	public IBinder onBind(Intent intent)
	{
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId)
	{
		// retrieve user ID
		Bundle extras = intent.getExtras();

		if (extras != null)
			mUserID = extras.getString(WelcomeActivity.USER_ID);

		// there are two possible scenarios:
		// - if content provider is empty, retrieve all data
		// - otherwise, carry on syncing operation
		if (mUserID == null)
		{
			stop();
		}
		else if (isContentProviderEmpty())
		{
			new GetItemsTask().execute(mUserID);
		}
		else
		{
			new SyncTask().execute();
		}

		// if the service is killed by system, leave it dead
		mIsRunning = true;
		return START_NOT_STICKY;
	}

	// disable runnign status and exit
	private void stop()
	{
		mIsRunning = false;

		// done inserting items, notify TodoListActivity
		Intent intent = new Intent(SYNC_SERVICE_ACTION);
		sendBroadcast(intent);

		stopSelf();
	}

	// upon the first login, ContentProvider will be empty
	// this function queries the content provider for available items
	private boolean isContentProviderEmpty()
	{
		ContentResolver resolver = getContentResolver();

		Uri uri = Uri.parse(TodoContract.MAIN_URI + "/item");
		Cursor cur = resolver.query(uri, new String[] {ItemTable.COLUMN_ID}, null, null, null);

		int count = cur.getCount();
		cur.close();
		return count == 0;
	}

	// extract items from JSONItem and insert them in ContentProvider
	private void saveItemsToCP(JSONObject obj)
	{
		Uri itemsUri = Uri.parse(TodoContract.MAIN_URI + "/item");

		try
		{
			JSONArray result = obj.getJSONArray(ItemAPI.FIELD_RESULT);
			for(int i = 0; i < result.length(); ++i)
			{
				ContentValues values = Formatter.unpackItem(result.getJSONObject(i));

				// set status to synced
				values.put(ItemTable.COLUMN_STATUS, Table.STATE_SYNCED);

				// insert in Content Provder
				getContentResolver().insert(itemsUri, values);
			}
		}
		catch (JSONException ex)
		{
			Log.e(SyncService.class.toString(), "Could not parse server response");
			ex.printStackTrace();
		}

		// we are done
		stop();
	}

	// reflect sync result on content provider
	private void reflectSyncOnCP(JSONObject result, int itemID)
	{
		// result with error -> could not sync
		// empty result -> successful delete
		// result with objectId -> successful insert
		// result with updatedAt -> successful update

		String objectID = null;
		String updatedAt = null;
		try
		{
			if (result.has("error"))
				return;

			if (result.has("objectId"))
				objectID = result.getString("objectId");

			if (result.has("updatedAt"))
				updatedAt = result.getString("updatedAt");
		}
		catch (JSONException ex)
		{
			Log.e(SyncService.class.toString(), "Could not parse server response");
			ex.printStackTrace();
		}

		ContentResolver resolver = getContentResolver();
		Uri uri = Uri.parse(TodoContract.MAIN_URI + "/item/" + String.valueOf(itemID));

		ContentValues values = new ContentValues();
		values.put(ItemTable.COLUMN_STATUS, Table.STATE_SYNCED);

		if (objectID != null)
			values.put(ItemTable.COLUMN_STR_ID, objectID);

		if (objectID == null && updatedAt == null)
			resolver.delete(uri, null, null);
		else
			resolver.update(uri, values, null, null);
	}

	private class GetItemsTask extends AsyncTask<String, Void, JSONObject>
	{
		@Override
		protected JSONObject doInBackground(String... params)
		{
			return ItemAPI.getAllItems(params[0]);
		}

		@Override
		protected void onPostExecute(JSONObject jsonObj)
		{
			saveItemsToCP(jsonObj);
		}
	}

	private class SyncTask extends AsyncTask<Void, Void, Void>
	{
		@Override
		protected Void doInBackground(Void... voids)
		{
			ContentResolver resolver = getContentResolver();

			// retrieve all unsynced items
			Uri uri = Uri.parse(TodoContract.MAIN_URI + "/item");
			String where = ItemTable.COLUMN_STATUS + " != ?";
			String[] params = new String[] {Table.STATE_SYNCED};

			Cursor cur = resolver.query(uri, TodoContract.Item.PROJECTION_ALL, where, params, null);
			if (cur.moveToFirst())
				do
				{
					// pack item into json
					JSONObject packedItem = Formatter.packItem(cur, mUserID);

					try
					{
						String method = packedItem.getString(ItemAPI.FIELD_METHOD);
						JSONObject obj = packedItem.getJSONObject(ItemAPI.FIELD_BODY);
						String url = packedItem.getString(ItemAPI.FIELD_PATH);

						// sync item with server then reflect changes on content provider
						JSONObject result = ItemAPI.syncData(obj, url, method);

						// itemID is needed to update items in content provider
						reflectSyncOnCP(result, Formatter.getItemID(cur));
					}
					catch (JSONException ex)
					{
						Log.e(SyncService.class.toString(), "Could not parse item");
						ex.printStackTrace();
					}

					cur.moveToNext();
				}
				while (!cur.isAfterLast());
			cur.close();

			return null;
		}

		@Override
		protected void onPostExecute(Void aVoid)
		{
			// we are done
			stop();
		}
	}
}
