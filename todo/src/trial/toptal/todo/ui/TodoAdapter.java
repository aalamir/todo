package trial.toptal.todo.ui;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import trial.toptal.todo.R;
import trial.toptal.todo.database.ItemTable;

public class TodoAdapter extends CursorAdapter
{
	// this interface provides a callback function for when an item
	// checkbox is clicked
	private CheckboxHandler mCheckHandler;
	private EditHandler mEditHandler;

	public TodoAdapter(Context context, Cursor c, CheckboxHandler checkHandler, EditHandler editHandler)
	{
		// use autoRequery to autmoatically respond to Content Provider
		// change notifications
		super(context, c, true);

		// mCheckHandler is called when the checkbox item is clicked
		mCheckHandler = checkHandler;

		// mEditHandler is called when the edit button is clicked
		mEditHandler = editHandler;
	}

	@Override
	public View newView(Context context, Cursor cursor, ViewGroup parent)
	{
		LayoutInflater inflater = LayoutInflater.from(parent.getContext());
		View retView = inflater.inflate(R.layout.todo_list_item, parent, false);

		return retView;
	}

	@Override
	public void bindView(View view, Context context, final Cursor cursor)
	{
		// get column ids
		int titleIndex = cursor.getColumnIndex(ItemTable.COLUMN_TITLE);
		int dueIndex = cursor.getColumnIndex(ItemTable.COLUMN_DUE);
		int doneIndex = cursor.getColumnIndex(ItemTable.COLUMN_DONE);
		int priorityIndex = cursor.getColumnIndex(ItemTable.COLUMN_PRIORITY);

		// set title, due date and isDone vlaues
		((TextView)view.findViewById(R.id.txtTitle)).setText(cursor.getString(titleIndex));
		((TextView)view.findViewById(R.id.txtDue)).setText(cursor.getString(dueIndex));
		((CheckBox)view.findViewById(R.id.chkDone)).setChecked(cursor.getInt(doneIndex) == 1);

		// figure out which priority image to use
		int priority = cursor.getInt(priorityIndex);
		int priorityRes = R.drawable.priority_low;
		if (priority == 1)
			priorityRes = R.drawable.priority_normal;
		else if (priority == 2)
			priorityRes = R.drawable.priority_high;

		((ImageView)view.findViewById(R.id.imgPrioirty)).setImageResource(priorityRes);

		// setup click handle for checkbox
		int idIndex = cursor.getColumnIndex(ItemTable.COLUMN_ID);
		final int itemID = cursor.getInt(idIndex);

		view.findViewById(R.id.chkDone).setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				mCheckHandler.onCheckboxClick(((CheckBox) v).isChecked(), itemID);
			}
		});

		// setup click handle for edit button
		final int pos = cursor.getPosition();
		view.findViewById(R.id.btnEdit).setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				mEditHandler.onEditClick(pos);
			}
		});
	}

	// this interface provides a callback function for when an item
	// checkbox is clicked
	public interface CheckboxHandler
	{
		public void onCheckboxClick(boolean newState, int itemID);
	}

	// this interface provides a callback function for editing an item
	public interface EditHandler
	{
		public void onEditClick(int pos);
	}
}
