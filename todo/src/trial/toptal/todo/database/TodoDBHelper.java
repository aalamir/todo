package trial.toptal.todo.database;

/*
	Database helper class
*/

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class TodoDBHelper extends SQLiteOpenHelper
{
	private static final String DATABASE_FILENAME = "todo.db";
	private static final int DATABASE_VERSION = 1;

	private List<Table> mTables = null;

	public TodoDBHelper(Context context)
	{
		super(context, DATABASE_FILENAME, null, DATABASE_VERSION);

		mTables = new ArrayList<>();
		mTables.add(new UserTable());
		mTables.add(new ItemTable());
	}

	// Method is called during creation of the database
	@Override
	public void onCreate(SQLiteDatabase database)
	{
		if (mTables == null)
			return;

		// create tables
		for (Table table : mTables)
			table.onCreate(database);
	}

	// Method is called during an upgrade of the database,
	// e.g. when database version is changed
	@Override
	public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion)
	{
		if (mTables == null)
			return;

		// upgrade tables
		for (Table table : mTables)
			table.onUpgrade(database, oldVersion, newVersion);
	}
}
